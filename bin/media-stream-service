#!/bin/bash

#  media-stream-service
#
#  Capture digital video broadcast from URL.
#  $Revision: 1.3 $
#
#  Copyright (C) 2023-2023 Jordi Pujol <jordipujolp AT gmail DOT com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#************************************************************************

set -o errexit -o nounset -o pipefail +o noglob +o noclobber

export PS4='+\t ${LINENO}:${FUNCNAME:+"${FUNCNAME}:"} '
#set -o xtrace
echo "${@}" >&2

_ps_children() {
	local ppid=${1:-${$}} \
		pid
	for pid in $(pgrep -P ${ppid}); do
		_ps_children ${pid}
		printf '%d ' ${pid}
	done
}

end="${1}"
shift
url="$(printf '%s\n' "${@}" | \
sed -nr -e '\|^(http://[^:]+:[[:digit:]]+/).*|{s||\1|;p;q}' \
-e '${q1}')" || {
	echo "Error: Invalid stream url. Exiting" >&2
	exit 1
}

while [ $(date +%s) -lt ${end} ]; do
	c=10
	while let c--; do 
		if wget --quiet -O /dev/stderr "${url}playlist.m3u"; then
			sleep 5
			"${@}" || :
		fi
		sleep 10
	done
done &
pid=${!}

while [ $(date +%s) -lt ${end} ] && \
kill -s 0 ${pid} 2> /dev/null; do
	sleep 60
done

kill -s KILL $(_ps_children) || :

exit 0
